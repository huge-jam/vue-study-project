import VueRouter from 'vue-router'

import Home from './components/tabbar/Home.vue'
import Member from './components/tabbar/Member.vue'
import Search from './components/tabbar/Search.vue'
import Shopcar from './components/tabbar/Shopcar.vue'

// 3. 创建路由对象
var router = new VueRouter({
  routes: [ // 配置路由规则
    {
      path: '/',
      redirect: 'home'
    },
    {
      path: '/home',
      component: Home
    },
    {
      path: '/member',
      component: Member
    },
    {
      path: '/search',
      component: Search 
    },
    {
      path: '/shopcar',
      component: Shopcar 
    },
  ],
  linkActiveClass: 'mui-active' // 覆盖默认的路由高亮的类
})

// 把路由对象暴露出去
export default router