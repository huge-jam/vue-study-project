# 项目简介

## 制作首页App组件

### 完成 Header 区域
使用Mint-UI组件

### 制作底部Tabbar 区域
使用MUI的Tabbar.html

### 制作中间区域
使用`router-view`.
制作购物车小图标的时候，需要使用`icons-extra.css`样式。注意MUI帮助文档和示例的使用。
